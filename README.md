# satsuki

## install

system requirements:
 - Crystal 0.27+ to build it, or not if you plan on cross-compiling to the
    target server's architecture
 - [pgmetrics](https://github.com/rapidloop/pgmetrics)
 - postgresql running on the server

## build

```
git clone https://gitlab.com/elixire/satsuki && cd satsuki
shards build
./bin/satsuki path/to/config.ini
```
