require "file"
require "influxdb"
require "process"
require "json"
require "enumerable"

def call_pgmetrics(log, config)
  pg_cfg = config["postgres"]
  pg_database = config["postgres"]["database"]

  tmp_path = File.tempfile("pgmetrics")

  if tmp_path == nil
    log.fatal("failed to make temporary file")
    return nil
  end

  stdin = IO::Memory.new("#{pg_cfg["password"]}\n")
  stdout = IO::Memory.new()
  stderr = IO::Memory.new()

  # this looks ugly but bear with me.
  task = Process.new(
    "pgmetrics", args: [
      # pgmetrics supports export to json which is exactly what we want!
      "-f", "json", "-o", tmp_path.path,
      "-h", pg_cfg["host"], "-U", pg_cfg["username"], pg_database
    ], input: stdin, output: stdout, error: stderr
  )

  log.debug("file: #{tmp_path.path}")

  status = task.wait

  if !status.success?
    log.fatal("failed to run pgmetrics: #{stdout.to_s} #{stderr.to_s}")
    return nil
  end

  return tmp_path
end

def submit(db, dp_label, dp : Int | Int64 | UInt32)
  db.write dp_label, dp
end

def submit(db, dp_label, dp : JSON::Any)
  # TODO: check timestamping on here
  db.write dp_label, dp.as_i64
end

def send_bgw(db, data)
  bgw = data["bg_writer"]

  submit db, "pg_bgw_checkpoints_req", bgw["checkpoints_req"]
  submit db, "pg_bgw_checkpoints_timed", bgw["checkpoints_timed"]

  submit db, "pg_bgw_buffers_backend", bgw["buffers_backend"]
  submit db, "pg_bgw_buffers_clean", bgw["buffers_clean"]
  submit db, "pg_bgw_buffers_checkpoint", bgw["buffers_checkpoint"]
end

def send_reads(db, metrics)
    submit db, "pg_tup_fetched", metrics["tup_fetched"]
    submit db, "pg_tup_returned", metrics["tup_returned"]

    submit db, "pg_temp_bytes", metrics["temp_bytes"]

    # big means bad
    submit db, "pg_numbackends", metrics["numbackends"]

    # hit means cached block, read means block read from disk
    # you want hit > read
    submit db, "pg_blks_hit", metrics["blks_hit"]
    submit db, "pg_blks_read", metrics["blks_read"]

    # row info
    submit db, "pg_tup_inserted", metrics["tup_inserted"]

    # high rates of those are bad
    submit db, "pg_tup_updated", metrics["tup_updated"]
    submit db, "pg_tup_deleted", metrics["tup_deleted"]
end

def send_tbl(db, data)
  total_dead_rows = 0
  
  data["tables"].as_a.each do |table|
    total_dead_rows += table["n_dead_tup"].as_i
  end

  submit db, "pg_dead_rows", total_dead_rows
end

def process_out(log, db, data, pg_database)
  log.debug("process data for db #{pg_database}")
  send_bgw db, data

  metrics = data["databases"][0]
  send_reads db, metrics
  send_tbl db, data
end

def reporter_tick(log, config, db)
  tmp = call_pgmetrics log, config

  if tmp.nil?
    log.fatal("error when calling pgmetrics, stopping")
    return false
  end

  outdata = JSON.parse(tmp)
  tmp.delete
  process_out log, db, outdata, config["postgres"]["database"]
  return true
end

def reporter(log, config)
  influx_cfg = config["influx"]

  if influx_cfg["ssl"] != ""
    sec = "s"
  else
    sec = ""
  end

  # construct the influx url. this is very different than aioinflux,
  # but it is what i get for using a 9 month old library with no documentation.
  influx_url = "http#{sec}://#{influx_cfg["host"]}:#{influx_cfg["port"]}"

  log.debug("influxdb url: #{influx_url}")

  # basic auth only happens if username isn't "", so this is slightly better
  # than what i did on aioinflux... however aioinflux also has the same
  # behavior. empty username = no auth, so, i guess i was doing it wrong
  # from the start.
  client = InfluxDB::Client.new(
    influx_url, influx_cfg["username"], influx_cfg["password"])

  db = client.databases[influx_cfg["database"]]

  while true
    log.debug("ticking")
    res = reporter_tick log, config, db

    if !res
      break
    end

    sleep 15
  end

  log.info("reporter loop stop")
end
