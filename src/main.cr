require "file"
require "errno"
require "ini"
require "logger"

require "./reporter"

begin
  config_data = File.read(ARGV[0])
  config = INI.parse(config_data)
rescue ex : Errno
  puts "err: #{ex.errno} #{ex.errno_message}"
rescue ex : IndexError
  puts "err: config path not provided"
end

if config.nil?
  puts "no config"
  exit(1)
end

# setup a logger
if config["satsuki"]["debug"] != ""
  level = Logger::DEBUG
else
  level = Logger::INFO
end

log = Logger.new(STDOUT, level)

# call the reporter function who will setup influxdb and the main loop
reporter(log, config)
